﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using Newtonsoft.Json;

namespace Raptoreum_Address_Verifier
{
    internal class Program
    {
        private static string WalletDirectory { get; set; }
        private static string CliDirectory { get; set; }
        private const string CLIFILE = "raptoreum-cli.exe";
        private const string CONFFILE = "raptoreum.conf";
        private static List<string> WinningAddresses { get; set; } = new List<string>();
        private static List<string> MyAddresses { get; set; } = new List<string>();

        private static void Main(string[] args)
        {
            if (!CliLocation())
            {
                Console.WriteLine("Please rerun this application in the same location as raptoreum-cli.exe");
                ExitApp(1);
            }

            WalletFolder();

            if (!CheckForConf())
            {
                Console.WriteLine("Conf file does not exist.  A basic one will be created and the wallet will need to be restarted as well as this program.");
                Console.Write("Would you like me to create one for you? (y or n): ");
                string choice = Console.ReadLine();
                while (!string.Equals(choice, "y", StringComparison.InvariantCultureIgnoreCase) && !string.Equals(choice, "n", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.Write("Invalid choice.  Try again: ");
                    choice = Console.ReadLine();
                }
                if (string.Equals(choice, "y", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (CreateConf())
                    {
                        Console.WriteLine("Conf file successfully created with localhost access only.  Please restart the wallet and this application.");
                        ExitApp(0);
                    }
                    else
                    {
                        Console.WriteLine("\nUnable to create conf file.  Please try manually creating a file named 'raptoreum.conf' in the datadir folder (where the wallet resides)");
                        Console.WriteLine("File contents should be as follows:\n" +
                            "server=1\n" +
                            "rpcuser=[username]\n" +
                            "rpcpassword=[password]\n" +
                            "rpcallowip=127.0.0.1\n" +
                            "Once created, restart the wallet and this application.");
                        ExitApp(1);
                    }
                }
                else if (string.Equals(choice, "n", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Cannot proceed.");
                    ExitApp(1);
                }
                else
                {
                    Console.WriteLine("Invalid choice.  Now exiting.");
                    ExitApp(1);
                }
            }

            GetWinningAddresses();

            if (WinningAddresses.Count == 0)
            {
                Console.WriteLine("No potential winning addresses were given.");
                ExitApp(1);
            }

            CheckAddresses();

            if (MyAddresses.Count == 0)
            {
                Console.WriteLine("\nSorry, you were not a winner.  Better luck next time!");
            }
            else
            {
                Console.WriteLine("\nCongratulations!  The following address(es) were winners:\n");
                foreach (string address in MyAddresses)
                {
                    Console.WriteLine(address);
                }
                Console.WriteLine("\n\nThanks for playing and helping contribute to the Raptoreum project.");
            }

            ExitApp(0);
        }

        private static bool CliLocation()
        {
            string fullPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            CliDirectory = Path.GetDirectoryName(fullPath);
            if (File.Exists(Path.Combine(CliDirectory, CLIFILE)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void WalletFolder()
        {
            bool validLocation = false;
            while (!validLocation)
            {
                Console.Write("Please enter the full path to your wallet folder: ");
                WalletDirectory = Console.ReadLine();

                if (File.Exists(Path.Combine(WalletDirectory, "wallet.dat")))
                {
                    validLocation = true;
                }
                else
                {
                    Console.WriteLine("Location given does not contain wallet.dat.  Try again!");
                }
            }
        }

        private static bool CheckForConf()
        {
            if (File.Exists(Path.Combine(WalletDirectory, CONFFILE)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void GetWinningAddresses()
        {
            WinningAddresses.Clear();
            Console.WriteLine("\nPlease enter winning address(es) and press enter after each one.\nIf copying/pasting from Discord, the line will already end with a carriage return.\nEnter 'q' to end the input:");

            string address = string.Empty;
            while (true)
            {
                address = Console.ReadLine();
                if (string.Equals(address, "q", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }
                else
                {
                    if (!string.Equals(address.Trim(), string.Empty))
                    {
                        WinningAddresses.Add(address.Trim());
                    }
                }
            }
        }

        private static void CheckAddresses()
        {
            MyAddresses.Clear();

            foreach (string addr in WinningAddresses)
            {
                try
                {
                    using (var proc = new Process())
                    {
                        proc.StartInfo = new ProcessStartInfo
                        {
                            FileName = Path.Combine(CliDirectory, CLIFILE),
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            RedirectStandardError = false,
                            CreateNoWindow = true,
                            Arguments = $"-datadir=\"{WalletDirectory}\" validateaddress {addr}"
                        };

                        StringBuilder sb = new StringBuilder();
                        proc.OutputDataReceived += (sender, args) => sb.AppendLine(args.Data);
                        proc.Start();
                        proc.BeginOutputReadLine();
                        proc.WaitForExit();

                        try
                        {
                            var result = JsonConvert.DeserializeObject<JsonResponse>(sb.ToString());
                            if (Convert.ToBoolean(result.IsMine))
                            {
                                MyAddresses.Add(addr);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Could not parse response from CLI.  Please check your address(es) for accuracy.");
                            Console.WriteLine($"Error: {ex.Message.ToString()}");
                            ExitApp(1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Something went terribly wrong executing the CLI command.");
                    ExitApp(1);
                }
            }
        }

        private static bool CreateConf()
        {
            string username = string.Empty;
            string pwd = string.Empty;

            Console.Write("\nPlease specify an RPC username: ");
            username = Console.ReadLine();
            Console.Write("Please specify an RPC password: ");
            pwd = Console.ReadLine();

            string[] lines = { "server=1", $"rpcuser={username}", $"rpcpassword={pwd}", "rpcallowip=127.0.0.1" };
            try
            {
                File.WriteAllLines(Path.Combine(WalletDirectory, CONFFILE), lines);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static void ExitApp(int exitCode)
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Environment.Exit(exitCode);
        }
    }

    internal class JsonResponse
    {
        public bool? IsValid { get; set; }
        public string Address { get; set; }
        public string ScriptPubKey { get; set; }
        public bool? IsMine { get; set; }
        public bool? IsWatchOnly { get; set; }
        public bool? IsScript { get; set; }
        public string PubKey {get;set; }
        public bool? IsCompressed {get;set; }
        public Int64? Timestamp {get;set; }
        public string HdKeyPath {get;set; }
        public string HdSeedId {get;set; }
    }
}