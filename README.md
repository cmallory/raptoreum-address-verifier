# Raptoreum Address Verifier

This console application only requires .NET 4.5 and is intended for Windows only.

No information is transmitted other than to your daemon and CLI.

1. Upon startup, you will be prompted for your wallet folder location so that it may use the raptoreum.conf file (you will have the option to automatically create one, if needed).  This is required for RPC communication with the QT wallet daemon.
2. Copy and paste the addresses given by the Raptoreum team that are the chosen addresses for that particular drawing.  Press enter after each address (copying/pasting from Discord will already have the carriage return line ending).  Press 'q' on an empty line to end entering addresses.
3. Each address will be submitted to your daemon through the CLI and checked to see if your wallet owns the winning address.
 

If the conf file is automatically created, or you want to verify contents before starting the application and wallet, check in the folder where the wallet.dat is located for raptoreum.conf and open it.
The following settings should be present:
* server=1
* rpcuser=[your username]
* rpcpassword=[your password]
* rpcallowip=127.0.0.1
     
The last setting will ensure that only connections and commands are accepted from your local machine for security.
If any changes were made to this file, the QT wallet (or daemon, if not using the GUI) must be restarted for changed to take effect.

_Disclaimer: This program is free to use, modify, and transfer.  It may not be free of bugs so manually verify addresses if in doubt._